# docker-webapp-template

A docker template for running a webapp connected to a mysql database, alongside with ackee analytics solution.

Different networks are being used in order to allow access to the services only *via* Nginx

It makes use of a [php-nginx image](https://gitlab.com/121593/docker-php-nginx-template) holding nginx and the actual webapp

`secrets` folder contains secrets examples. It should also contains a subfolder `ssl` containing SSL certificates and keys used by the application.


## Usage

####Dev
`docker-compose up -d`

####Prod
`docker stack deploy -c compose.yml`

## Services
 - [Ackee](https://github.com/electerious/Ackee) : Lightweight, privacy-friendly analytics solution
 - MongoDB, needed by Ackee
 - Mysql database used by the app
 - [php-nginx](https://gitlab.com/121593/docker-php-nginx-template) Webserver and actual webapp
