DROP USER IF EXISTS 'app';
DROP USER IF EXISTS 'beta_app';

CREATE USER 'app'@'%' IDENTIFIED BY 'app_db_password';
CREATE USER 'beta_app'@'%' IDENTIFIED BY 'beta_app_db_password';

CREATE DATABASE IF NOT EXISTS app;
CREATE DATABASE IF NOT EXISTS beta_app;

GRANT ALL ON app.* TO 'app'@'%' WITH GRANT OPTION ;
GRANT ALL ON beta_app.* TO 'beta_app'@'%' WITH GRANT OPTION ;
